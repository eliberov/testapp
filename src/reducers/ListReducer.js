'use strict'

export function listReducer(state = {
  success: [],
  errors: [],
  list: []
}, action) {

  switch( action.type ){
    case 'ADD_LIST_ITEM':
      const reversedInput = action.item.reverse()
      const exists = state.list.some( item => {
        return item.value === reversedInput;
      })
      const duplicateList = [ ...state.list ]
      if( !exists ){
        duplicateList.push({
          value: reversedInput,
          entered: new Date()
        })
      }
      return Object.assign( {}, state, { list: duplicateList });
    case 'REMOVE_LIST_ITEM':

      const idx = state.list.indexOfKey( 'value', action.item.value )
      const slicedList = idx > -1 ? [ ...state.list.slice( 0, idx ), ...state.list.slice( idx+1 ) ] : state.list

      return Object.assign( {}, state, { list: slicedList });
  }
  return state;
}