'use strict'

import React from 'react';
import { connect } from 'react-redux'
import ListItem from '../components/ListItem'

class MainApp extends React.Component {

  constructor(props){
    super(props)
    this.checkKeyUp = this.checkKeyUp.bind( this )
  }

  checkKeyUp(event){
    if( event.keyCode == 13 ) {
      this.props.dispatch({ type: 'ADD_LIST_ITEM', item: event.target.value })
    }
  }

  removeListItem(item){
    this.props.dispatch({ type: 'REMOVE_LIST_ITEM', item: item })
  }

  render() {
    const { list } = this.props
    return (
      <div className="container">
        <h4> Message Board </h4>
        <div className="form">
          <input type="text" onKeyUp={ this.checkKeyUp } placeholder="Enter a message" />
          {list.map( ( item, key ) =>
            <ListItem key={ key } item={ item } removeItem={ this.removeListItem.bind( this, item ) } />
          )}
        </div>
      </div>
    )
  }
}

MainApp.contextTypes = {
}

function mapStateToProps(state) {
  const { listReducer } = state

  const {
    list
  } = listReducer

  return {
    list
  }
}

export default connect(mapStateToProps)(MainApp)