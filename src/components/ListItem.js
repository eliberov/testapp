'use strict'

import React from 'react';
import moment from 'moment';

class ListItem extends React.Component {

  constructor(props){
    super(props)
    this.removeItem = this.removeItem.bind( this )
  }

  removeItem(){
    this.props.removeItem()
  }

  formatDate( date ){
    return moment( date ).format("MMMM DD, YYYY");
  }

  render() {
    const { item } = this.props
    return (
      <div className="item">
        <div className="contents">
          <p>{item.value}</p>
          <p>{ this.formatDate( item.entered ) }</p>
        </div>
        <div className="remove" onClick={ this.removeItem }>
          <span className="glyphicon glyphicon-remove" aria-hidden="true"></span>
        </div>
      </div>
    )
  }
}

ListItem.contextTypes = {
}

export default ListItem