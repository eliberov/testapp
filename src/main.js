require( 'styles/main.css' );
require( './lib/strings' );
require( './lib/arrays' );

import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux'
import MainApp from './containers/MainApp'
import configureStore from './stores/main.store'

const store = configureStore();
ReactDOM.render(
  <Provider store={store}>
    <MainApp />
  </Provider>
  , document.getElementById('app')
);
