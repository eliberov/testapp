
import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from './../reducers/'
import defaultMiddlware from './../middleware/';

const createStoreWithMiddleware = compose(
  applyMiddleware( defaultMiddlware )
)(createStore);

export default function configureStore(initialState) {
  const store = createStoreWithMiddleware( rootReducer, initialState );
  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept( './../reducers', () => {
      const nextRootReducer = require('./../reducers').default;
      store.replaceReducer(nextRootReducer);
    });
  }
  return store;
}