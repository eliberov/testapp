/** Reverse a string
 *
 *
 */
String.prototype.reverse = String.prototype.reverse || (function(){
  var strArray = this.split('');
  strArray.reverse();

  var strReverse = strArray.join('');

  return strReverse;
})
