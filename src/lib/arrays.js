

Array.prototype.indexOfKey = Array.prototype.indexOfKey || (function(attr, value) {
    for(var i = 0; i < this.length; i++) {
        if(this[i].hasOwnProperty(attr) && this[i][attr] === value) {
            return i;
        }
    }
    return -1;
})